> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# Mobile App Development & Management using Android Studio and PHP

## Dong Min Hong    

### LIS 4381 Requirements:

*Course Work Links:*

1. [A1 README.md](a1/README.md)
    - Install AAMPS
    - Install JDK and run java Hello
    - Install Android Studio and run My First App
    - Provide screenshots of installations
    - Create BitBucket repository links
    - Git commands with short descriptions
    
 2. [A2 README.md](a2/README.md)
    - Create bruschetta recipe application 
    - Create additional link to a button
    
 3. [A3 README.md](a3/README.md)
    - MySQL Workbench ERD
    - Forward Engineer 
    - Create a launcher icon image and display it in both activities
    - Create concert mobile app
    - Create link to a3.mwb and a3.sql
    
4. [P1 README.md](p1/README.md)
    - Make mobile friendly application
    - Make business card
    - Create a launcher icon image
    - Create detail page about myself

5. [A4 README.md](a4/README.md)

    - Create a link by using PHP
    - Create fill in boxes for petstore
    - Warn the user when there is no input

6. [A5 README.md](a5/README.md)

    - Create a link by using PHP
    - Link the database to the website
    - Warn the user when there is no input
    - Add data in the petstore
    - Create database

7. [P2 README.md](p2/README.md)

    - Create a link by using PHP
    - Link the database to the website
    - Apply edit and delete function
    - Warn the user when there is no input
    




 

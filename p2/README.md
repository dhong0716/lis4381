# LIS4381

## Dong Min Hong

### Project 2 # Requirements:

*Sub-Heading:*

1. Link database to the server
2. Forward Engineer 
3. Add data in the petstore
4. Create a link
5. Give edit or delete function to the users




#### Assignment Screenshots:

*Home Screen for A5*:

![Home Screen](img/p2.PNG)

*Edit_petstore.php*:

![Edit_petstore.php](img/p21.PNG)

*Edit_petstore_process.php*:

![Edit_petstore_process.php](img/p22.PNG)

*Home page*:

![Home Page](img/home.PNG)

*RSS Feed*:

![RSS Feed](img/rss.PNG)



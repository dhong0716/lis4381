> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4381

## Dong Min Hong

### Assignment 2 Requirements:

*Three parts:*

1. Create layout in Android Studio
2. Push to Bitbucket
3. Chapter Questions

#### README.md file should include the following items:

* Screenshot of running application’s first user interface
* Screenshot of running application’s second user interface

#### Assignment Screenshots:

*Screenshot of running application’s first user interface*:

![First User Interface Screenshot](img/firstui.png)

*Screenshot of running application’s second user interface*:

![Second User Interface Screenshott](img/secondui.png)



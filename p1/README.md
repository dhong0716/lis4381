> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381

## Dong Min Hong

### P1 # Requirements:

*Sub-Heading:*

1. Create launcer icon
2. Create a Business Card
3. Create 2 page information about myself


#### Assignment Screenshots:

*Screenshot of Business Card Intro

![Business Card Intro](img/p1.png)

*Screenshot of Business Card Details*:

![Business Details](img/p12.png)

